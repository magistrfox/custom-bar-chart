import { useState, useEffect, useRef } from 'react';
import styles from './BarChart.module.scss';
import { mock } from './BarChartMockUp';
import BarChartD3 from './BarChartD3';

const BarChart = () => {
  const svgRef = useRef();

  const [data] = useState(mock);

  useEffect(() => {
    if (svgRef.current) {
      BarChartD3(data, svgRef);
    }
  }, [data]);

  return (
    <div className={styles.box}>
      <svg width="100%" height="100%" ref={svgRef}></svg>
    </div>
  );
};

export default BarChart;
