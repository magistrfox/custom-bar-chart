const mock = [
  {
    name: 'name-1', value: 2000
  },
  {
    name: 'name-2', value: 3000
  },
  {
    name: 'name-3', value: 1500
  },
  {
    name: 'name-4', value: 500
  },
  {
    name: 'name-5', value: 4000
  },
  {
    name: 'name-6', value: 1400
  }
]

export {mock}