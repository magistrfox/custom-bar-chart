import { select, scaleLinear, max, scaleBand, axisLeft, axisBottom } from 'd3';

const BarChartD3 = (data, svgRef) => {
  const svg = select(svgRef.current);
  const width = parseFloat(
    window.getComputedStyle(svgRef.current.parentElement).width
  );
  const height = parseFloat(
    window.getComputedStyle(svgRef.current.parentElement).height
  );

  const margin = { left: 50, top: 20, right: 20, bottom: 50 };
  const innerWidth = width - margin.left - margin.right;
  const innerHeight = height - margin.top - margin.bottom;

  const XScale = scaleLinear()
    .domain([0, max(data.map((data) => data.value))])
    .range([0, innerWidth]);

  const YScale = scaleBand()
    .domain(data.map((data) => data.name))
    .range([0, innerHeight])
    .padding(0.3);

  const chartGroup = svg.append('g');

  chartGroup.attr('transform', `translate(${margin.left}, ${margin.top})`);

  chartGroup.append('g').call(axisLeft(YScale));
  chartGroup
    .append('g')
    .call(axisBottom(XScale))
    .attr('transform', `translate(0, ${innerHeight})`);

  chartGroup
    .selectAll('rect')
    .data(data)
    .join((enter) => enter.append('rect'))
    .attr('y', (data) => YScale(data.name))
    .attr('fill', '#17C')
    .attr('width', (data) => XScale(data.value))
    .attr('height', (data) => YScale.bandwidth());
};

export default BarChartD3;
