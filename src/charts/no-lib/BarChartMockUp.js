const mock = [
  {
    name: 'name-1',
    value: 2000,
  },
  {
    name: 'name-2',
    value: 3000,
  },
  {
    name: 'name-3',
    value: 1500,
  },
  {
    name: 'name-4',
    value: 500,
  },
  {
    name: 'name-5',
    value: 4000,
  },
  {
    name: 'name-6',
    value: 700,
  },
  {
    name: 'name-7',
    value: 1000,
  },
  {
    name: 'name-8',
    value: 3000,
  },
  {
    name: 'name-9',
    value: 300,
  },
  {
    name: 'name-10',
    value: 200,
  },
  {
    name: 'name-11',
    value: 250,
  },
  {
    name: 'name-12',
    value: 2000,
  },
  {
    name: 'name-13',
    value: 3200,
  },
  {
    name: 'name-14',
    value: 1500,
  },
  {
    name: 'name-15',
    value: 500,
  },
  {
    name: 'name-16',
    value: 4000,
  },
  {
    name: 'name-17',
    value: 700,
  },
  {
    name: 'name-18',
    value: 1000,
  },
  {
    name: 'name-19',
    value: 3000,
  },
  {
    name: 'name-20',
    value: 300,
  },
  {
    name: 'name-21',
    value: 200,
  },
  {
    name: 'name-22',
    value: 250,
  },
];

export { mock };
