import { useState, useEffect, useRef } from 'react';
import styles from './BarChart.module.scss';
import { mock } from './BarChartMockUp';

const BarChart = () => {
  const maxWidth = Math.max(
    ...mock.map((item) => {
      return item.value;
    })
  );

  let ticks = [];
  ticks[4] = 0;
  ticks.fill(0, 0, 4);
  ticks = ticks.map((tick, i) => {
    return (maxWidth / ticks.length) * i;
  });
  ticks.push(maxWidth);

  const allBarsRef = useRef(null);

  const [width, setWidth] = useState(0);

  useEffect(() => {
    if (allBarsRef.current) {
      setWidth(parseInt(window.getComputedStyle(allBarsRef.current).width));
    }
  }, [width]);

  return (
    <div className={styles.chartBox}>
      <div className={styles.barBox}>
        <div className={styles.allBars} ref={allBarsRef}>
          {mock.map((item, i) => (
            <div
              style={{
                width: `${(item.value / maxWidth) * (width - 100)}px`,
                height: '30px',
              }}
              key={i}
              className={styles.singleBar}
            >
              <span className={styles.singleBar__value}>{item.value}</span>
              <span className={styles.singleBar__name}>{item.name}</span>
            </div>
          ))}
        </div>
        <div className={styles.yAxes} />
        <div className={styles.xAxesBox}>
          <div className={styles.xAxes} />
          <div className={styles.ticksBox}>
            {ticks.map((tick, i) => (
              <div key={i}>{tick}</div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default BarChart;
