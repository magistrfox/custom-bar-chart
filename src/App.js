import BarChart from './charts/no-lib/BarChart';

function App() {
  return (
    <div className="App">
      <BarChart />
    </div>
  );
}

export default App;
