# Custom bar chart component

[Link](https://magistrfox.gitlab.io/custom-bar-chart)

## Inspired by

- [this article](https://medium.com/nuances-of-programming/%D0%BF%D0%BE%D1%87%D0%B5%D0%BC%D1%83-%D1%8F-%D0%B1%D0%BE%D0%BB%D1%8C%D1%88%D0%B5-%D0%BD%D0%B5-%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D1%83%D1%8E%D1%81%D1%8C-d3-js-371ec627f153)
